/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Keyboard } from 'react-native';

import Redux from '~/store/Redux';
import validationSchema from './validation';

import {
  Container, Title, LoginButton, Input, Password, FormWrapper, Wrapper, ErrorText,
} from './styles';

const initialValues = {
  name: '',
  email: '',
  birthDate: '',
  password: '',
};

class Main extends Component {
  handleSubmit = (values) => {
    const { getAuthRequest } = this.props;
    getAuthRequest(values);
  }

  render() {
    const { navigation, auth: { shouldNavigate, loading, error } } = this.props;
    if (shouldNavigate) return navigation.navigate('Main');
    return (
      <Wrapper onPress={Keyboard.dismiss} accessible={false}>
        <Container>
          <Title accessibility="Titulo">Bem-vindo!</Title>
          <FormWrapper
            initialValues={initialValues}
            onSubmit={this.handleSubmit}
            enableReinitialize={false}
            validateOnChange={false}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              values,
              errors,
              handleChange,
            }) => (
              <>
                <Input
                  autoComplete="none"
                  autoCapitalize="words"
                  keyboardType="email-address"
                  id="lg_name_input"
                  accessibility="Campo de nome para realizar login"
                  label="Nome"
                  error={errors.name}
                  value={values.name}
                  onChangeText={handleChange('name')}
                />
                <Input
                  autoComplete="none"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  id="lg_email_input"
                  accessibility="Campo de e-mail para realizar login"
                  label="E-mail"
                  error={errors.email}
                  value={values.email}
                  onChangeText={handleChange('email')}
                />
                <Input
                  keyboardType="numeric"
                  label="Aniversário (DD/MM/AAAA)"
                  id="birthDate"
                  error={errors.birthDate}
                  value={values.birthDate}
                  onChangeText={handleChange('birthDate')}
                  onSubmitEditing={handleSubmit}
                  maskType="datetime"
                  options={{
                    format: 'DD/MM/YYYY',
                  }}
                />
                <Password
                  id="lg_password_input"
                  accessibility="Campo de senha para realizar login"
                  keyboardType="decimal-pad"
                  label="Senha"
                  returnKeyType="send"
                  error={errors.password}
                  value={values.password}
                  onChangeText={handleChange('password')}
                  onSubmitEditing={handleSubmit}
                />
                {error && (
                <ErrorText
                  id="error"
                  accessibility="Usuário ou senha inválidos"
                >
                  Usuário ou senha inválidos
                </ErrorText>
                )}
                <LoginButton
                  id="lg_button"
                  accessibility="Botão para realizar login"
                  loading={loading}
                  onPress={handleSubmit}
                >
                ENTRAR
                </LoginButton>
              </>
            )}
          </FormWrapper>
        </Container>
      </Wrapper>
    );
  }
}

export default Redux(Main);
