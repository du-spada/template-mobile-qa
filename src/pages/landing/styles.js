import styled from 'styled-components/native';
import {
  Button,
  Typography,
  TextInput,
  PasswordInput,
} from '@platformbuilders/react-native-ui';
import { Formik } from 'formik';
import { colors } from '~/styles';

export const Wrapper = styled.TouchableWithoutFeedback`
    flex: 1;
`;

export const Container = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: ${colors.secundary};
`;

export const Title = styled(Typography).attrs({ variant: 'largeTitle' })`
    font-weight: bold;
    text-align: center;
    color: ${colors.primary};
`;

export const LoginButton = styled(Button).attrs({ secondary: true })`
    width: 232px;
    margin-top: 45px;
    align-self: center;
`;

export const Input = styled(TextInput)`
    color: #000;
    width: 80%;
`;

export const Password = styled(PasswordInput)`
    width: 80%;
`;

export const FormWrapper = styled(Formik)`
  margin-bottom: 15px;
  justify-content: center;
`;

export const ErrorText = styled(Title).attrs({ variant: 'body' })`
    color: ${colors.danger};
`;
