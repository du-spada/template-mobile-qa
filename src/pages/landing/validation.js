import * as FormValidator from 'yup';
import DateManager from 'moment';

const validateAlphanumeric = new RegExp('^[a-zA-Z0-9.]*$');
const validateLength = (value) => (value && value.length >= 6) || false;
const validateBirthDate = (value) => {
  if (value) {
    return DateManager(value, 'DD/MM/YYYY')
      .add(18, 'years')
      .isBefore();
  }
  return true;
};

export default FormValidator.object().shape({
  name: FormValidator.string()
    .required('Nome obrigatório'),
  email: FormValidator.string()
    .required('E-mail obrigatório')
    .email('E-mail inválido'),
  password: FormValidator.string()
    .matches(validateAlphanumeric, 'Senha não pode conter caracteres especiais')
    .test('length', 'Senha deve conter pelo menos 6 dígitos', validateLength)
    .required('Senha obrigatória'),
  birthDate: FormValidator.string()
    .required('Informe seu aniversário') // desabilitar para subir pra apple
    .test('date', 'Você deve ter pelo menos 18 anos', validateBirthDate),
});
