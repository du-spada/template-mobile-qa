/* eslint-disable react/prop-types */
import React from 'react';
import DateManager from 'moment';
import Redux from '~/store/Redux';

import { Container, Title, LargeTitle } from './styles';

const Main = ({ auth }) => (
  <Container>
    <LargeTitle id="title" accessibility="Título">
    Você logou!
    </LargeTitle>
    <Title id="name" accessibility="Campo de nome">
    Nome: {auth.name}
    </Title>
    <Title id="e-mail" accessibility="Campo de e-mail">
    E-mail: {auth.email}
    </Title>
    <Title id="birthDate" accessibility="Campo de aniversário">
      Aniversário: {DateManager(auth.birthDate, 'DDMMYYYY').format('DD/MM/YYYY')}
    </Title>
    <Title id="password" accessibility="Campo de senha">
    Senha: {auth.password}
    </Title>
  </Container>
);

export default Redux(Main);
