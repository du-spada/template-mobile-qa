import styled from 'styled-components/native';
import {
  Button,
  Typography,
} from '@platformbuilders/react-native-ui';
import { colors, metrics } from '~/styles';

export const Container = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: ${colors.secundary};
`;

export const Title = styled(Typography).attrs({ variant: 'headline' })`
    align-items: center;
    justify-content: center;
    color: ${colors.primary};
    margin: ${metrics.baseMargin}px;
`;

export const LargeTitle = styled(Title).attrs({ variant: 'largeTitle' })`
    margin-bottom: ${metrics.baseMargin * 2}px;
`;

export const BackButton = styled(Button).attrs({ secondary: true })`
    width: 232px;
    margin-top: 45px;
    align-self: center;
`;
