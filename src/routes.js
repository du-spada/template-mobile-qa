import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Landing from '~/pages/landing';
import Main from '~/pages/main';

const Routes = createAppContainer(createSwitchNavigator({ Landing, Main }));

export default Routes;
