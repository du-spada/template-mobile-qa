export const Types = {
  AUTH_REQUEST: 'auth/AUTH_REQUEST',
  AUTH_SUCCESS: 'auth/AUTH_SUCCESS',
  AUTH_FAILURE: 'auth/AUTH_FAILURE',
};

const initialState = {
  name: '',
  email: '',
  birthDate: '',
  password: '',
  loading: false,
  error: false,
  errorMessage: '',
  shouldNavigate: false,
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case Types.AUTH_REQUEST:
      return {
        name: action.payload.name,
        email: action.payload.email,
        birthDate: action.payload.birthDate,
        password: action.payload.password,
        loading: true,
        error: false,
        errorMessage: '',
        shouldNavigate: false,
      };
    case Types.AUTH_SUCCESS:
      return {
        ...state,
        loading: false,
        shouldNavigate: true,
      };
    case Types.AUTH_FAILURE:
      return {
        ...initialState,
        error: true,
        loading: false,
        errorMessage: action.payload.error,
        shouldNavigate: false,
      };
    default:
      return state;
  }
}

export const Creators = {
  getAuthRequest: (values) => ({
    type: Types.AUTH_REQUEST,
    payload: { ...values },
  }),
  getAuthSuccess: () => ({
    type: Types.AUTH_SUCCESS,
  }),
  getAuthFailure: (error) => ({
    type: Types.AUTH_FAILURE,
    payload: { error },
  }),
};
