import { put } from 'redux-saga/effects';
import { Creators as AuthAction } from '~/store/ducks/auth';

export function* getAuth(credentials) {
  if (credentials.payload.email === 'qa@platformbuilders.io'
  && credentials.payload.password === '123456') {
    yield put(AuthAction.getAuthSuccess());
  } else {
    yield put(AuthAction.getAuthFailure('Erro. Verifique suas credenciais.'));
  }
}
